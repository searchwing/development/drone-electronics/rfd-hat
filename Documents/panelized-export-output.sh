#!/bin/zsh

KICAD_CLI_COMMAND=kicad-cli-nightly # kicad-cli or kicad-cli-nightly
PRO_NAME=rfd-hat

# Enter script location directory
cd $(dirname "$0")

# Define output directories
OUT=./panelized-output          # output dir
FAB=${OUT}/Fabrication          # fabrication files dir
GER=${FAB}/Gerbers              # gerber files dir
PANEL=${OUT}/panelized-pcb      # panel pcb files dir

PANEL_PCB=panelized-rfd-hat.kicad_pcb

# create output dir
rm -drf ${OUT}
echo "=> Creating output directories: ${GER}, ${PANEL}"
mkdir -p ${GER}
mkdir -p ${PANEL}

# panellize layout
kikit panelize \
    --layout 'hspace: 3mm; vspace: 3mm; hbackbone: 4mm; vbackbone: 4mm; rows: 3; cols: 5' \
    --cuts 'type: mousebites; spacing: 0.75mm' \
    ../rfd-hat.kicad_pcb ${PANEL}/${PANEL_PCB}

# Export Schematics
echo "=> Exporting schematics"
${KICAD_CLI_COMMAND} sch export pdf -o ${OUT}/SCH-${PRO_NAME}.pdf ../${PRO_NAME}.kicad_sch 

# Export layer buildup pdf
echo "=> exporting layers to multiple pdf's"
${KICAD_CLI_COMMAND} pcb export pdf --ibt -l F.Cu ${PANEL}/${PANEL_PCB} -o "F.tmp.pdf"
${KICAD_CLI_COMMAND} pcb export pdf --ibt -l B.Cu ${PANEL}/${PANEL_PCB} -o "B.tmp.pdf"
${KICAD_CLI_COMMAND} pcb export pdf --ibt -l F.Fab ${PANEL}/${PANEL_PCB} -o "F.FAB.tmp.pdf"
${KICAD_CLI_COMMAND} pcb export pdf --ibt -l B.Fab ${PANEL}/${PANEL_PCB} -o "B.FAB.tmp.pdf"
## Merge layers to one pdf
echo "=> merging layer pdf's to single pdf"
gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=${OUT}/LAYERS-${PRO_NAME}.pdf F.tmp.pdf F.FAB.tmp.pdf B.tmp.pdf B.FAB.tmp.pdf
echo "=> deleting single-layer pdf's"
rm *tmp.pdf

# Export Fabrication files
## Gerbers
echo "=> exporting gerber files"
${KICAD_CLI_COMMAND} pcb export gerbers --layers F.Cu,B.Cu,F.Mask,B.Mask,F.Paste,B.Paste,F.SilkS,B.SilkS,Edge.Cuts --no-x2 --output ${GER}/ ${PANEL}/${PANEL_PCB}
## Drill
echo "=> exporting drill files"
${KICAD_CLI_COMMAND} pcb export drill --format excellon --excellon-zeros-format suppressleading --units mm --excellon-min-header --generate-map --map-format ps --output ${GER}/ ${PANEL}/${PANEL_PCB}
## Position
echo "=> exporting position files"
${KICAD_CLI_COMMAND} pcb export pos --side front --format ascii --units mm --smd-only --exclude-fp-th --output ${GER}/${PRO_NAME}-front.pos ${PANEL}/${PANEL_PCB}
${KICAD_CLI_COMMAND} pcb export pos --side back --format ascii --units mm --smd-only --exclude-fp-th --output ${GER}/${PRO_NAME}-back.pos ${PANEL}/${PANEL_PCB}
## Zip Fabrication files
echo "=> zipping fabrication files"
zip -j ${FAB}/gerber-files ${GER}/*

## BOM
echo "=> exporting xml BOM"
${KICAD_CLI_COMMAND} sch export python-bom ../${PRO_NAME}.kicad_sch --output ./searchwing-py-bom/BOM-${PRO_NAME}.xml
echo "=> creating xlsx BOM"
cd searchwing-py-bom
python3 searchwing_bom.py  "BOM-${PRO_NAME}.xml" "out.csv"
echo "=> deleting temporary bom files"
rm out.csv BOM-${PRO_NAME}.xml
mv out.xlsx ../${FAB}/BOM-${PRO_NAME}.xlsx
cd ..

