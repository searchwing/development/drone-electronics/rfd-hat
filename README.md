# rfd-hat

This PCB is used to connect the RFD868 telemetry module via two JST-GH connectors.

![picture](Documents/images/rfd-hat-3d.png)

## panelize pcb via script

The script `Documents/panelized-export-output.sh` automatically uses the kikit-cli to pannelize the pcb-layout abd exports schematics, pdf-layout, BOM and production files.

Kicad 7.0 is required.

## panelize the pcb via GUI

In order to panelize the PCB, the plugin KiKit has to be installed via the plugin manager.

Due to a BUG KiKit is only useable from a fresh and empty PCB file. Open the kicad pcb editor to create the PCB Panel.

In order to create the PCB with the size below 100x100 mm (cheap at JLCPCB) the following JSON settings were used:

```
{
    "layout": {
        "hspace": "3mm",
        "vspace": "3mm",
        "hbackbone": "4mm",
        "vbackbone": "4mm",
        "rows": "3",
        "cols": "5"
    },
    "cuts": {
        "type": "mousebites",
        "spacing": "0.75mm"
    }
}
```



